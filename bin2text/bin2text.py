def onlybits(s):
    """
    Helper function to remove anything but 0s and 1s from a string.

      >>> onlybits('010110')
      '010110'
      >>> onlybits('010110 10011')
      '01011010011'
    """
    bitstr = ''
    for ch in s:
        if ch in '01':
            bitstr += ch

    return bitstr 


def bin2text(s):
    """
      >>> bin2text('01001100')
      'L'
      >>> bin2text('01001100 01101001')
      'Li'
    """
    bitstr = onlybits(s)
    text = ''

    for i in range(0, len(bitstr), 8):
        text += chr(int(bitstr[i:i+8], 2))

    return text


if __name__ == '__main__':
    import doctest
    doctest.testmod()
