def add_em_up(a, b, c):
    """
      >>> add_em_up(1, 1, 1)
      3
      >>> add_em_up(3, 8, 5)
      16
      >>> add_em_up(-2, -2, 4)
      0
    """
    return a + b + c


def say_hi_to(name, lang):
    """
      >>> say_hi_to('Andrea', 'es')
      '¡Hola, Andrea!'
      >>> say_hi_to('Colton', 'fr')
      'Bonjour, Colton!'
      >>> say_hi_to('Jane', 'en')
      'Hello, Jane!'
      >>> say_hi_to('Meiji', 'es')
      '¡Hola, Meiji!'
    """
    if lang == 'es':
        return f'¡Hola, {name}!'
    if lang == 'en':
        return f'Hello, {name}!'
    return f'Bonjour, {name}!'


def middle_value(n1, n2, n3):
    """
      >>> middle_value(3, 8, 5)
      5
      >>> middle_value(6, 9, 4)
      6
      >>> middle_value(3, 7, 1)
      3
      >>> middle_value(7, 7, 7)
      7
    """
    if n1 > n2:
        if n2 > n3:
            return n2
        return n3
    # n1 <= n2
    if n2 < n3:
        return n2
    # n1 <= n2 and n3 <= n2, n2 is max
    if n1 > n3:
        return n1
    return n3


def silly_sentence(job, adjective):
    """
      >>> silly_sentence('lumberjack', 'okay')
      "I'm a lumberjack and I'm okay."
      >>> silly_sentence('student', 'real smart')
      "I'm a student and I'm real smart."
      >>> silly_sentence('truck driver', 'sleepy')
      "I'm a truck driver and I'm sleepy."
    """
    return f"I'm a {job} and I'm {adjective}."


def add_first_two(a, b, c):
    """
      >>> add_first_two(1, 1, 1)
      2
      >>> add_first_two(3, 8, 5)
      11
      >>> add_first_two(-2, -2, 4)
      -4
    """
    return a + b


def has_a_match(a, b, c):
    """
      >>> has_a_match(1, 1, 1)
      True
      >>> has_a_match(3, 8, 5)
      False
      >>> has_a_match(3, 8, 8)
      True
      >>> has_a_match(9, 8, 3)
      False
      >>> has_a_match(3, 8, 3)
      True
    """
    return a == b or a == c or  b == c


if __name__ == "__main__":
    import doctest
    doctest.testmod()
