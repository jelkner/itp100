def has_letters(word, letters):
    """
    Check if all the required characters to write word are found in letters.

    >>> has_letters('happy', 'abchijppy')
    True
    >>> has_letters('happy', 'abchijpy')
    False
    >>> has_letters('admiring', 'findinganagrams')
    True
    >>> has_letters('soup', 'findinganagrams')
    False
    """
    word = word.lower()
    letters = letters.lower()
    for letter in word:
        if word.count(letter) > letters.count(letter):
            return False
    return True


def make_combinations(data_list):
    """
    Recursively generate all combinations of elements from data_list.

      >>> combinations = make_combinations(['a', 'b', 'c', 'd'])
      >>> ['a', 'c', 'd'] in combinations 
      True
      >>> len(combinations)
      16
      >>> ['a', 'b', 'd'] in combinations 
      True
    """
    if len(data_list) == 0:
        return [[]]
    combs = []
    for comb in make_combinations(data_list[:-1]):
        combs += [comb, comb + [data_list[-1]]]
    return combs


def match_length(word_list, target_length):
    """
    Return a list of lists of combinations of words in word_list whose combined
    lengths equal the given target_length.

      >>> words =  ['the', 'be', 'far', 'am', 'this', 'cake', 'on']
      >>> matches = match_length(words, 7)
      >>> ['the', 'be', 'am'] in matches
      True
      >>> ['the', 'this'] in matches
      True
      >>> ['the', 'cake'] in matches
      True
      >>> ['the', 'be', 'on'] in matches
      True
      >>> ['the', 'am', 'on'] in matches
      True
      >>> ['far', 'am', 'on'] in matches
      True
      >>> all([isinstance(elem, list) for elem in matches])
      True
    """
    matches = []

    for candidate in make_combinations(word_list):
        if len(''.join(candidate)) == target_length:
            matches.append(candidate)

    return matches


if __name__ == "__main__":
    import doctest
    doctest.testmod()
