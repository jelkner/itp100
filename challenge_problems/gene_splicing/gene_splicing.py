def mix_solutions(A, B, C, T):
    """
      >>> mix_solutions(32, 16, 1, 0.00001)
      145
      >>> mix_solutions(32, 16, 4, 0.00001)
      38
      >>> mix_solutions(300, 20, 2, 0.02)
      93
    """
    target = A / B
    b_mix = C / (B + C)
    a_mix = ((A - C) + b_mix * C) / A
    num_iters = 1

    while True:
        a_diff = a_mix / (1 - a_mix) - target
        b_diff = b_mix / (1 - b_mix) - target

        if a_diff < T and b_diff > -T:
            break

        b_mix = (b_mix * B + a_mix * C) / (B + C)
        a_mix = (a_mix * (A - C) + b_mix * C) / A
        num_iters += 1

    return num_iters


if __name__ == "__main__":
    import doctest
    doctest.testmod()
