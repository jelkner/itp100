from string import ascii_lowercase as lower
from string import ascii_uppercase as upper


def make_cipher_text(s, shift):
    """
      >>> make_cipher_text("The quick brown fox jumped over the lazy dogs", 1)
      'Uif rvjdl cspxo gpy kvnqfe pwfs uif mbaz epht'
      >>> make_cipher_text("stuff greater then A but less than a []^_`", 20)
      'mnozz alyunyl nbyh U von fymm nbuh u []^_`'
    """
    ctext = ''

    for ch in s:
        if ch in lower:
            ctext += lower[(ord(ch) - ord('a') + shift) % 26]
        elif ch in upper:
            ctext += upper[(ord(ch) - ord('A') + shift) % 26]
        else:
            ctext += ch

    return ctext


if __name__ == "__main__":
    import doctest
    doctest.testmod()
