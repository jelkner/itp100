#!/usr/bin/env python3
import sys
from caesarcipher import make_cipher_text

def main():
    try:
        f = open(sys.argv[1])
        lines = f.readlines()
        f.close()
        shift = int(lines[0].strip())
        assert(0 <= shift <= 25)
    except (AssertionError, ValueError):
        print(
            "First line in input file must contain a number between 0 and 25"
        )
        return 
    except IndexError:
        print("Please enter input file name after program name.")
    except FileNotFoundError:
        print(f"File {sys.argv[1]} does not exist.")

    for line in lines[1:]:
        if line == "STOP\n":
            break
        print(make_cipher_text(line, shift), end='')


if __name__ == "__main__":
    main()
