import sys

DEBUG = False

houses = ['Gryffindor', 'Ravenclaw', 'Slytherin', 'Hufflepuff']
counts = []

if len(sys.argv) == 2:
    fname = sys.argv[1]
else:
    fname = input("Enter filename of registration list: ")

f = open(fname)

player_houses = [line.split(',')[1].strip() for line in f.readlines()]
f.close()

for house in houses:
    counts.append(player_houses.count(house))

out_str = ''

for index, value in enumerate(counts):
    if value < 7:
        out_str += f'{houses[index]} does not have enough players.\n'
    elif value > 7:
        out_str += f'{houses[index]} has too many players.\n'

print(out_str if out_str else "List complete, let’s play quidditch!")

if DEBUG:
    print(f'counts: {counts}')
