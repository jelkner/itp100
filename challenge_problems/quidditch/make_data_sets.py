# Create data files with 28 random names and houses
import random

if __name__ == "__main__":
    fin = open('names.txt')
    names = [name.strip() for name in fin.readlines()]
    fin.close()

    houses = ['Gryffindor', 'Ravenclaw', 'Slytherin', 'Hufflepuff']

    fout = open('registration_list.txt', 'w')

    for i in range(28):
        name = random.choice(names)
        names.remove(name)
        fout.write(f'{name}, {random.choice(houses)}\n')
    
