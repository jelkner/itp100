import re

f = open('re_matching.txt')
lines = f.readlines()
f.close()

rexp = re.compile('\d\d')

matches = [line for line in lines if re.search(rexp, line)]
print(matches)
