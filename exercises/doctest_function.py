def join_together(this, that):
    """
    This will be our first example of using doctest.

      >>> join_together('happy', 'birthday')
      'happybirthday'
    """


if __name__ == '__main__':
    import doctest
    doctest.testmod()
