# Regular Expression Quick Guide

RE  | Matches
--- | -------
^ | the *beginning* of a line
$ | the *end* of a line
. | any character
\s | whitespace
\S | any non-whitespace character
\* | repeats of any character zero or more times
\*? | repeats of any character zero or more times (non-greedy)
\+ | repeats of a character one or more times
\+? | repeats of a character one or more times (non-greedy)
[aeiou] | a single character in the listed set 
[\^XYZ] | a single character *not* in the listed set 
[a-z0-9] | a character within the ranges a to z and 0 to 9 
( | start of string extraction
) | end of string extraction
