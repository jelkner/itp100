import time

from alex_primes_refactored import primes_between


min_val = int(input(f"Lowest number in range to search for primes: "))
max_val = int(input(f"Highest number in range to search for primes: "))
start_time = time.time()
prime_list = primes_between(min_val, max_val)
elapsed_time = time.time() - start_time

print(f"Counted {len(prime_list)} primes between {min_val} and {max_val}: ")
print(prime_list)
print(f"in {elapsed_time} seconds.")
