import math


def is_prime(n):
    """
    Returns True if n is a prime number and False otherwise.

      >>> is_prime(2)
      True
      >>> is_prime(5)
      True
      >>> is_prime(10)
      False
      >>> is_prime(21)
      False
      >>> is_prime(43)
      True
      >>> is_prime(49)
      False
    """
    if n == 2:
        return True

    if n <= 1 or n % 2 == 0:
        return False

    # if n not prime, its smallest factor must be less than or equal n ** 0.5
    min_fact = int(n ** 0.5) + 1

    # with only even prime already checked, check odds from n to min_fact
    for d in range(3, min_fact, 2):
        if n % d == 0:
            return False

    return True


def primes_between(low, high):
    """
    Returns a list of prime numbers between low and high and the time needed
    to compute the list.
    
      >>> primes_between(10, 20)
      [11, 13, 17, 19]
      >>> primes_between(20, 32)
      [23, 29, 31]
    """
    return [n for n in range(low, high + 1) if is_prime(n)]


if __name__ == "__main__":
    import doctest

    doctest.testmod()
