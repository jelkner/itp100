import math
import time

primeCount = 0
# intiallization
tmin = float(
    input("What is the lowest number you would like to check to see if it is prime?")
)
max = float(
    input("What is the highest number you would like to check to see if it is prime?")
)
# Gets the highest and lowest number to be checked
while tmin > max:
    print(
        "The first number you entered is larger than the second. Please renter the numbers."
    )
    tmin = float(
        input(
            "What is the lowest number you would like to check to see if it is prime?"
        )
    )
    max = float(
        input(
            "What is the highest number you would like to check to see if it is prime?"
        )
    )
min = tmin
if min.is_integer() == False:
    min = math.floor(min)
if 0 <= min < 2:
    min = 2
    print("1 is neither prime nor composite.")
if min < 0:
    min = 2
    print("All negative numbers are composite")
    print("1 is neither prime nor composite.")

start_time = time.time()
# grabs the time in seconds
for numChecked in range(int(min), int(max) + 1):
    # main for loop
    prime = True
    # this value checks if the number that is being checked is a prime
    checkRange = range(2, math.floor(numChecked / 2) + 1)
    # sets the range of numbers that the number being checked will be divided by
    for n in checkRange:
        if (numChecked / n).is_integer():
            # checks if the number being checked makes an integer when divided by the number it is being checked with.
            prime = False
            break
            # this break is not neccisary but cuts down on the time the program takes drastically
    if prime == True:
        # cheks if a number has been found to be a prime
        print(str(numChecked) + (" is a prime number"))
        primeCount = primeCount + 1
        # adds to the prime count
end_time = time.time()
# grabs the time in seconds at the end of the program
print(
    ("counted ")
    + str(primeCount)
    + (" prime numbers between ")
    + str(tmin)
    + (" and ")
    + str(max)
    + (" and took ")
    + str(end_time - start_time)
    + (" seconds")
)
# tells the user how long it took the program to run and how many prime numbers were found.
